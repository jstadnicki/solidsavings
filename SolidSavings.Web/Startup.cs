﻿namespace SolidSavings.Web
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;

    using SolidSavings.Web.Controllers;
    using SolidSavings.Web.Database;
    using SolidSavings.Web.Logic;

    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore()
                    .AddRazorViewEngine();
            services.AddScoped<IBusiness, Business>();
            services.AddScoped<IUserBusiness, UserBusiness>();
            services.AddScoped<IDatabase, InMemoryDatabase>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Incomes}");
            });
        }
    }
}
