﻿namespace SolidSavings.Web.Models
{
    using System.ComponentModel.DataAnnotations;

    using SolidSavings.Web.Models.Enums;

    public class LoginDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public LoginActionType Action { get; set; }

        public UserRegistrationType RegistrationType { get; set; }
    }
}