﻿namespace SolidSavings.Web.Models
{
    using System;

    public class Outcome
    {
        public decimal Netto { get; set; }

        public int Year { get; internal set; }

        public int Month { get; internal set; }

        public Guid Id { get; internal set; }
    }
}