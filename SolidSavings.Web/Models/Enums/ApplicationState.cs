﻿namespace SolidSavings.Web.Models.Enums
{
    public enum ApplicationState
    {
        Income,
        OutCome,
        Report
    }

    public enum UserRegistrationType
    {
        Free,
        Paid,
        Demo
    }
}