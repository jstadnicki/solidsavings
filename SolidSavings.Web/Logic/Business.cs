﻿namespace SolidSavings.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using SolidSavings.Web.Database;
    using SolidSavings.Web.Logic;
    using SolidSavings.Web.Models;

    public class Business : IBusiness
    {
        private readonly IDatabase database;

        public Business(IDatabase database)
        {
            this.database = database;
        }

        public void AddIncome(Guid currentUserId, AddIncomeDto dto)
        {
            this.database.UsersIncomes[currentUserId].Add(
                new Income { Id = Guid.NewGuid(), Netto = dto.IncomeNetto, Year = dto.Year, Month = dto.Month });
        }

        public void AddOutcome(Guid currentUserId, AddOutcomeDto dto)
        {
            this.database.UsersOutcomes[currentUserId].Add(
                new Outcome { Id = Guid.NewGuid(), Netto = dto.OutcomeNetto, Year = dto.Year, Month = dto.Month });
        }

        public List<Income> GetCircleIncomesData(Guid currentUserId)
        {
            var incomes = this.database.UsersIncomes.Single(kp => kp.Key == currentUserId).Value
                .GroupBy(i => new { Y = i.Year, M = i.Month }, i => i.Netto)
                .Select(g => new Income { Year = g.Key.Y, Month = g.Key.M, Netto = g.Sum(s => s) }).OrderBy(i => i.Year)
                .ThenBy(i => i.Month).ThenByDescending(i => i.Netto).ToList();

            return incomes;
        }

        public List<Outcome> GetCircleOutcomesData(Guid currentUserId)
        {
            var outcomes = this.database.UsersOutcomes.Single(kp => kp.Key == currentUserId).Value
                .GroupBy(i => new { Y = i.Year, M = i.Month }, i => i.Netto)
                .Select(g => new Outcome { Year = g.Key.Y, Month = g.Key.M, Netto = g.Sum(s => s) })
                .OrderBy(i => i.Year).ThenBy(i => i.Month).ThenByDescending(i => i.Netto).ToList();
            return outcomes;
        }

        public List<Income> Incomes(Guid currentUserId)
        {
            var incomes = this.database.UsersIncomes.Single(kp => kp.Key == currentUserId).Value
                .GroupBy(i => new { Y = i.Year, M = i.Month }, i => i.Netto)
                .Select(g => new Income { Year = g.Key.Y, Month = g.Key.M, Netto = g.Sum(s => s) }).OrderBy(i => i.Year)
                .ThenBy(i => i.Month).ThenByDescending(i => i.Netto).ToList();
            return incomes;
        }

        public List<Income> GetLineIncomesData(Guid currentUserId)
        {
            var incomes = this.database.UsersIncomes.Single(kp => kp.Key == currentUserId).Value
                .GroupBy(i => new { Y = i.Year, M = i.Month }, i => i.Netto)
                .Select(g => new Income { Year = g.Key.Y, Month = g.Key.M, Netto = g.Sum(s => s) }).OrderBy(i => i.Year)
                .ThenBy(i => i.Month).ThenByDescending(i => i.Netto).ToList();
            return incomes;
        }

        public List<Outcome> GetLineOutcomesData(Guid currentUserId)
        {
            var outcomes = this.database.UsersOutcomes.Single(kp => kp.Key == currentUserId).Value
                .GroupBy(i => new { Y = i.Year, M = i.Month }, i => i.Netto)
                .Select(g => new Outcome { Year = g.Key.Y, Month = g.Key.M, Netto = g.Sum(s => s) })
                .OrderBy(i => i.Year).ThenBy(i => i.Month).ThenByDescending(i => i.Netto).ToList();

            return outcomes;
        }

        public List<Income> ListIncomes(Guid currentUserId)
        {
            var incomes = this.database.UsersIncomes.Single(kp => kp.Key == currentUserId).Value.OrderBy(i => i.Year)
                .ThenBy(i => i.Month).ThenByDescending(i => i.Month).ToList();
            return incomes;
        }

        public List<Outcome> ListOutcomes(Guid currentUserId)
        {
            var outcomes = this.database.UsersOutcomes.Single(kp => kp.Key == currentUserId).Value.OrderBy(i => i.Year)
                .ThenBy(i => i.Month).ThenByDescending(i => i.Month).ToList();

            return outcomes;
        }

        public List<Outcome> Outcomes(Guid currentUserId)
        {
            var outcomes = this.database.UsersOutcomes.Single(kp => kp.Key == currentUserId).Value
                .GroupBy(i => new { Y = i.Year, M = i.Month }, i => i.Netto)
                .Select(g => new Outcome { Year = g.Key.Y, Month = g.Key.M, Netto = g.Sum(s => s) })
                .OrderBy(i => i.Year).ThenBy(i => i.Month).ThenByDescending(i => i.Netto).ToList();

            return outcomes;
        }

        public List<Income> GetRadarIncomesData(Guid currentUserId)
        {
            var incomes = this.database.UsersIncomes.Single(kp => kp.Key == currentUserId).Value
                .GroupBy(i => new { Y = i.Year, M = i.Month }, i => i.Netto)
                .Select(g => new Income { Year = g.Key.Y, Month = g.Key.M, Netto = g.Sum(s => s) }).OrderBy(i => i.Year)
                .ThenBy(i => i.Month).ThenByDescending(i => i.Netto).ToList();

            return incomes;
        }

        public List<Outcome> GetRadarOutcomesData(Guid currentUserId)
        {
            var outcomes = this.database.UsersOutcomes.Single(kp => kp.Key == currentUserId).Value
                .GroupBy(i => new { Y = i.Year, M = i.Month }, i => i.Netto)
                .Select(g => new Outcome { Year = g.Key.Y, Month = g.Key.M, Netto = g.Sum(s => s) })
                .OrderBy(i => i.Year).ThenBy(i => i.Month).ThenByDescending(i => i.Netto).ToList();

            return outcomes;
        }

        public void RemoveOutCome(Guid currentUserId, Guid id)
        {
            var outcomeToRemove = this.database.UsersOutcomes[currentUserId].Single(i => i.Id == id);
            this.database.UsersOutcomes[currentUserId].Remove(outcomeToRemove);
        }

        public void RemoveIncome(Guid currentUserId, Guid id)
        {
            var incomeToRemove = this.database.UsersIncomes[currentUserId].Single(i => i.Id == id);
            this.database.UsersIncomes[currentUserId].Remove(incomeToRemove);
        }       
    }
}