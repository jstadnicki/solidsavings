﻿namespace SolidSavings.Web.Logic
{
    using System;
    using System.Collections.Generic;

    public interface IUserBusiness
    {
        KeyValuePair<Guid, string> FindUser(string dtoUsername);

        Guid RegisterNewUser(string username);
    }
}