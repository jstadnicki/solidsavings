﻿namespace SolidSavings.Web.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using SolidSavings.Web.Database;
    using SolidSavings.Web.Models;

    public class UserBusiness : IUserBusiness
    {
        private readonly IDatabase database;

        public UserBusiness(IDatabase database)
        {
            this.database = database;
        }

        public KeyValuePair<Guid, string> FindUser(string dtoUsername)
        {
            var keyValuePairs = this.database.Users.Where(
                f => dtoUsername.Equals(f.Value, StringComparison.InvariantCultureIgnoreCase)).ToList();
            if (keyValuePairs.Count == 1)
            {
                return keyValuePairs.Single();
            }

            return new KeyValuePair<Guid, string>(Guid.Empty, string.Empty);
        }

        public Guid RegisterNewUser(string username)
        {
            var newUserId = Guid.NewGuid();
            this.database.Users.Add(newUserId, username);
            this.database.UsersIncomes[newUserId] = new List<Income>();
            this.database.UsersOutcomes[newUserId] = new List<Outcome>();
            return newUserId;
        }
    }
}