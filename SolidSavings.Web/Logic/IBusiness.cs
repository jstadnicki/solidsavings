﻿namespace SolidSavings.Web.Logic
{
    using System;
    using System.Collections.Generic;

    using SolidSavings.Web.Models;

    public partial interface IBusiness
    {
        void AddIncome(Guid currentUserId, AddIncomeDto dto);

        void AddOutcome(Guid currentUserId, AddOutcomeDto dto);

        List<Income> GetCircleIncomesData(Guid currentUserId);

        List<Outcome> GetCircleOutcomesData(Guid currentUserId);

        List<Income> Incomes(Guid currentUserId);

        List<Income> GetLineIncomesData(Guid currentUserId);

        List<Outcome> GetLineOutcomesData(Guid currentUserId);

        List<Income> ListIncomes(Guid currentUserId);

        List<Outcome> ListOutcomes(Guid currentUserId);

        List<Outcome> Outcomes(Guid currentUserId);

        List<Income> GetRadarIncomesData(Guid currentUserId);

        List<Outcome> GetRadarOutcomesData(Guid currentUserId);

        void RemoveOutCome(Guid currentUserId, Guid id);

        void RemoveIncome(Guid currentUserId, Guid id);
    }
}