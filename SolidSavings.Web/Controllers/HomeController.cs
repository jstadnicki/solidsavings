﻿namespace SolidSavings.Web.Controllers
{
    using System;

    using Microsoft.AspNetCore.Mvc;

    using SolidSavings.Web.Infrastructure;
    using SolidSavings.Web.Logic;
    using SolidSavings.Web.Models;

    [SolidAuthorization]
    public class HomeController : Controller
    {
        private readonly IBusiness business;

        public HomeController(IBusiness business) => this.business = business;

        public static Guid CurrentUserId { get; set; }

        public static string CurrentUserName { get; set; }

        [HttpGet]
        public IActionResult AddIncome()
        {
            return this.View();
        }

        [HttpPost]
        public IActionResult AddIncome(AddIncomeDto dto)
        {
            if (this.ModelState.IsValid)
            {
                this.business.AddIncome(CurrentUserId, dto);
                return this.RedirectToAction("Incomes");
            }

            return this.View("AddIncome");
        }

        [HttpGet]
        public IActionResult AddOutcome()
        {
            return this.View();
        }

        [HttpPost]
        public IActionResult AddOutcome(AddOutcomeDto dto)
        {
            if (this.ModelState.IsValid)
            {
                this.business.AddOutcome(CurrentUserId, dto);
                return this.RedirectToAction("Outcomes");
            }

            return this.View("AddOutcome");
        }

        [HttpGet]
        public IActionResult Circle()
        {
            var incomes = this.business.GetCircleIncomesData(CurrentUserId);
            var outcomes = this.business.GetCircleOutcomesData(CurrentUserId);

            var circleChartModel = new ChartModel { Incomes = incomes, Outcomes = outcomes, };
            return this.View("Circle", circleChartModel);
        }

        [HttpGet]
        public IActionResult Incomes()
        {
            var incomes = this.business.Incomes(CurrentUserId);
            return this.View("Incomes", incomes);
        }

        [HttpGet]
        public IActionResult Line()
        {
            var incomes = this.business.GetLineIncomesData(CurrentUserId);
            var outcomes = this.business.GetLineOutcomesData(CurrentUserId);

            var lineChartModel = new ChartModel { Incomes = incomes, Outcomes = outcomes, };
            return this.View("Line", lineChartModel);
        }

        [HttpGet]
        public IActionResult ListIncomes()
        {
            var incomes = this.business.ListIncomes(CurrentUserId);
            return this.View("ListIncomes", incomes);
        }

        [HttpGet]
        public IActionResult ListOutcomes()
        {
            var outcomes = this.business.ListOutcomes(CurrentUserId);
            return this.View("ListOutcomes", outcomes);
        }

        [HttpGet]
        public IActionResult Outcomes()
        {
            var outcomes = this.business.Outcomes(CurrentUserId);
            return this.View("Outcomes", outcomes);
        }

        [HttpGet]
        public IActionResult Radar()
        {
            var incomes = this.business.GetRadarIncomesData(CurrentUserId);
            var outcomes = this.business.GetRadarOutcomesData(CurrentUserId);

            var radarChartModel = new ChartModel { Incomes = incomes, Outcomes = outcomes, };

            return this.View("Radar", radarChartModel);
        }

        [HttpGet]
        public IActionResult RemoveIncome(Guid id)
        {
            this.business.RemoveIncome(CurrentUserId, id);
            return this.RedirectToAction("ListIncomes");
        }

        [HttpGet]
        public IActionResult RemoveOutcome(Guid id)
        {
            this.business.RemoveOutCome(CurrentUserId, id);
            return this.RedirectToAction("ListOutcomes");
        }

        [HttpGet]
        public IActionResult Reports()
        {
            return this.View("Reports");
        }
    }
}