﻿namespace SolidSavings.Web.Controllers
{
    using System;

    using Microsoft.AspNetCore.Mvc;

    using SolidSavings.Web.Infrastructure;
    using SolidSavings.Web.Logic;
    using SolidSavings.Web.Models;
    using SolidSavings.Web.Models.Enums;

    public class UserController : Controller
    {
        private readonly IUserBusiness userBusiness;

        public UserController(IUserBusiness userBusiness)
        {
            this.userBusiness = userBusiness;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return this.View("Login");
        }

        public IActionResult Register()
        {
            return this.View("Register");
        }

        [HttpPost]
        public IActionResult Register(LoginDto dto)
        {
            if (!this.ModelState.IsValid)
            {
                return this.RedirectToAction("Register");
            }

            this.userBusiness.RegisterNewUser(dto.Username);

            return this.View("Login");
        }

        [HttpPost]
        public IActionResult Login(LoginDto dto)
        {
            if (!this.ModelState.IsValid)
            {
                return this.RedirectToAction("Login");
            }

            var userToLogin = this.userBusiness.FindUser(dto.Username);

            HomeController.CurrentUserName = userToLogin.Value;
            HomeController.CurrentUserId = userToLogin.Key;
            return this.RedirectToAction("Incomes", "Home");
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HomeController.CurrentUserName = string.Empty;
            HomeController.CurrentUserId = Guid.Empty;
            return this.RedirectToAction("Login");
        }
    }
}