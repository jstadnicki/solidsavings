﻿namespace SolidSavings.Web.Infrastructure
{
    using System;

    using Microsoft.AspNetCore.Mvc.Filters;

    public class SolidAnonymousAttribute : Attribute, IFilterMetadata
    {
    }
}