﻿namespace SolidSavings.Web.Infrastructure
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;

    using SolidSavings.Web.Controllers;

    public class SolidAuthorizationAttribute : Attribute, IAsyncAuthorizationFilter
    {
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (context.Filters.Any(f => f.GetType() == typeof(SolidAnonymousAttribute)))
            {
                return;
            }

            if (HomeController.CurrentUserId == Guid.Empty)
            {
                context.Result = new RedirectToActionResult("Login", "User", null);
            }
        }
    }
}