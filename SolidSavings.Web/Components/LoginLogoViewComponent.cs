﻿namespace SolidSavings.Web.Components
{
    using Microsoft.AspNetCore.Mvc;

    using SolidSavings.Web.Controllers;
    using SolidSavings.Web.Models.Enums;

    public class LoginLogoViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(ApplicationState state)
        {
            return this.View(state);
        }
    }
}