﻿namespace SolidSavings.Web.Database
{
    using System;
    using System.Collections.Generic;

    using SolidSavings.Web.Models;

    public class InMemoryDatabase : IDatabase
    {
        private static Dictionary<Guid, string> users;

        private static Dictionary<Guid, List<Income>> usersIncomes;

        private static Dictionary<Guid, List<Outcome>> usersOutcomes;

        static InMemoryDatabase()
        {
            InMemoryDatabase.users = new Dictionary<Guid, string>();
            InMemoryDatabase.usersIncomes = new Dictionary<Guid, List<Income>>();
            InMemoryDatabase.usersOutcomes = new Dictionary<Guid, List<Outcome>>();

            var newUserId = Guid.NewGuid();
            InMemoryDatabase.users.Add(newUserId, "demo");
            InMemoryDatabase.usersIncomes[newUserId] = new List<Income>();
            InMemoryDatabase.usersOutcomes[newUserId] = new List<Outcome>();

            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 10, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 10, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 11, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 12, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 12, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 4, Netto = 4000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 5, Netto = 2055 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 1, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 2, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 3, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 1, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 1, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 2, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 8, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 9, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 9, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 3, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 5, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 11, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 4, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 9, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 9, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 9, Netto = 2030 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 10, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 10, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 1, Netto = 21000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 2, Netto = 2020 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 11, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 12, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 4, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 5, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 6, Netto = 1000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 7, Netto = 6000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2002, Month = 8, Netto = 2500 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2000, Month = 9, Netto = 2000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 9, Netto = 2040 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 10, Netto = 2001 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 11, Netto = 12000 });
            InMemoryDatabase.usersIncomes[newUserId].Add(new Income { Id = Guid.NewGuid(), Year = 2001, Month = 12, Netto = 0 });

            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 10, Netto = 12000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 10, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 11, Netto = 22000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 12, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 12, Netto = 42000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 4, Netto = 4000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 5, Netto = 2055 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 1, Netto = 62000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 2, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 3, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 1, Netto = 42000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 1, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 2, Netto = 22000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 8, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 9, Netto = 72000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 9, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 3, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 5, Netto = 72000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 6, Netto = 52000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 11, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 4, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 9, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 9, Netto = 12000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 9, Netto = 2030 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 10, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 10, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 1, Netto = 21000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 2, Netto = 2020 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 11, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 12, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 4, Netto = 12000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 5, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 6, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 6, Netto = 1000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 7, Netto = 6000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2002, Month = 8, Netto = 2500 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2000, Month = 9, Netto = 2000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 9, Netto = 12040 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 10, Netto = 2001 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 11, Netto = 12000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 12, Netto = 1000 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2001, Month = 12, Netto = 1111 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2002, Month = 12, Netto = 1222 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2003, Month = 12, Netto = 1333 });
            InMemoryDatabase.usersOutcomes[newUserId].Add(new Outcome { Id = Guid.NewGuid(), Year = 2004, Month = 12, Netto = 1444 });
        }

        public Dictionary<Guid, string> Users
        {
            get => users;
            set => users = value;
        }

        public Dictionary<Guid, List<Income>> UsersIncomes
        {
            get => usersIncomes;
            set => usersIncomes = value;
        }

        public Dictionary<Guid, List<Outcome>> UsersOutcomes
        {
            get => usersOutcomes;
            set => usersOutcomes = value;
        }
    }
}