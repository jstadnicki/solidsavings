﻿namespace SolidSavings.Web.Database
{
    using System;
    using System.Collections.Generic;

    using SolidSavings.Web.Models;

    public interface IDatabase
    {
        Dictionary<Guid, string> Users { get; set; }

        Dictionary<Guid, List<Income>> UsersIncomes { get; set; }

        Dictionary<Guid, List<Outcome>> UsersOutcomes { get; set; }
    }
}